#include <TFile.h>
#include <TCanvas.h>
#include <TH1.h>
#include <string>
#include <iostream>
#include <vector>


char* strToChar(std::string textString){
  static char textChar[100]; //may need to adjust depending on file path length
  textString.copy(textChar, textString.size()+1);
  textChar[textString.size()] = '\0';
  return textChar;
}

TH1F* getHist(TFile* file, std::string histName, int fileNum){
  TH1F *hist = (TH1F*) file -> Get(strToChar(histName));
  hist -> Scale(1./hist->Integral());
  if(fileNum == 1) hist -> SetLineColor(kRed);
  else if(fileNum == 2) hist -> SetLineColor(kGray);  
  return hist;
}


void PlottingMacro() {
  
  std::vector<std::string> filepaths = {"./final/hist-ZPrime750.root", "./final/hist-ZPrime2500.root", "./final/hist-ttbar_lep.root"};
  std::vector<std::string> objects = {"lep", "el", "mu", "jet"};
  std::vector<std::string> kinematics = {"pt", "eta", "phi"};
  std::vector<std::string> miscNames = {"met", "met_phi"}; // Additional histograms not fitting "h_[obj]_[kin]" format
  std::string histName, canvName;

  const int nFiles = filepaths.size();
  const int nObj = objects.size();
  const int nKin = kinematics.size();
  const int nMisc = miscNames.size();
  const int canvHeight=600, canvWidth=600; // Order may be reversed?
  
  TH1F* histograms[nFiles][nObj][nKin];
  TH1F* miscHists[nFiles][nMisc];
  TCanvas* canvases[nObj][nKin];
  TCanvas* miscCanv[nMisc];
  TCanvas* activeCanv;


  
  // Read files, create histograms and canvases
  for(int i=0; i<nFiles; i++){
    TFile *file = TFile::Open(strToChar(filepaths[i]), "READ");
    for(int j=0; j<nObj; j++){
      for(int k=0; k<nKin; k++){
	histName = "h_" + objects[j] + "_" + kinematics[k];
	canvName = "c_" + objects[j] + "_" + kinematics[k];
	histograms[i][j][k] = getHist(file, histName, i);
	if(i==0) canvases[j][k] = new TCanvas(strToChar(canvName), "", canvHeight, canvWidth);
      }
    }
    for(int l=0; l<nMisc; l++){
      histName = "h_" + miscNames[l];
      canvName = "c_" + miscNames[l];
      miscHists[i][l] = getHist(file, histName, i);
      if(i==0) miscCanv[l] = new TCanvas(strToChar(canvName), "", canvHeight, canvWidth);
    }
  }
  
  
  // Draw histograms to canvases and write to pdf
  for(int j=0; j<nObj; j++){
    for(int k=0; k<nKin; k++){
      activeCanv = canvases[j][k];
      activeCanv -> cd();
      for(int i=0; i<nFiles; i++) histograms[i][j][k] -> Draw("histsame");
      if(j==0 && k==0) activeCanv -> Print("output.pdf(");
      else if(miscNames.size()==0 && (j==nObj-1 && k==nKin-1)) activeCanv -> Print("output.pdf)");
      else activeCanv -> Print("output.pdf");
      delete activeCanv;
    }
  }
  for(int l=0; l<nMisc; l++){
    activeCanv = miscCanv[l];
    activeCanv -> cd();
    for(int i=0; i<nFiles; i++) miscHists[i][l] -> Draw("histsame");
    if(l == nMisc-1) activeCanv -> Print("output.pdf)");
    else activeCanv -> Print("output.pdf");
    delete activeCanv;
  }
  
}
